import { AssetsManager, Canvas, Sprite } from "@render/main";
import { Camera, Engine, Game } from "@core/main";
import { Vector, Block } from "@physics/main";
import { KeyPress } from "@input/main";
import { World } from "@world/main";
import { Player } from "@entities/main";

const elem = document.getElementById("enginex");
let canvas;

if (elem) {
  canvas = new Canvas(elem as HTMLCanvasElement);
}

class ExampleGame extends Game {
  private player!: Player;

  constructor(
    private world: World = new World(),
    private camera: Camera = new Camera({
      position: new Vector(new Block(0), new Block(0)),
      width: Block.fromPixels(canvas.width),
      height: Block.fromPixels(canvas.height),
    })
  ) {
    super();
    AssetsManager.addTexture(
      "ground",
      "https://thumbs.dreamstime.com/b/ground-pattern-pixel-art-illustration-seamless-swatches-panel-vector-classical-technique-67039967.jpg"
    );
    AssetsManager.addTexture("player_east", "/dist/assets/player_east.png");
    AssetsManager.addTexture("player_west", "/dist/assets/player_west.png");
    AssetsManager.addTexture("player_north", "/dist/assets/player_north.png");
    AssetsManager.addTexture("player_south", "/dist/assets/player_south.png");
    AssetsManager.addTexture("player", "/dist/assets/player.png");
    AssetsManager.addTexture("tree", "/dist/assets/tree.png");
  }

  setup(): void {
    this.player = new Player({
      position: new Vector(new Block(0), new Block(0)),
    });
  }

  loop(): void {
    KeyPress.loop();
    const elements = this.world.getElementsInRange(this.camera);

    elements.forEach((element) => {
      canvas.render(
        element,
        this.camera.getRelativePosition(element),
        new Vector(element.width, element.height)
      );
    });

    canvas.render(
      this.player,
      this.camera.getRelativePosition(this.player),
      new Vector(this.player.width, this.player.height)
    );

    const collideableEntities = [this.player, ...elements].filter(
      (entity) => entity.collideable
    );

    collideableEntities.forEach((collideableEntity) => {
      let isColliding = false;
      collideableEntities.forEach((otherEntity) => {
        if (
          collideableEntity !== otherEntity &&
          collideableEntity.collides(otherEntity)
        ) {
          isColliding = true;
          if (collideableEntity?.["onCollide"]) {
            collideableEntity?.["onCollide"](otherEntity);
          }
        }
      });
      collideableEntity.isColliding = isColliding;
    });

    this.camera.anchorTo(this.player);
  }
}

const game = new ExampleGame();

Engine.frameRate(24);

Engine.start(game);
