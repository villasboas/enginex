import { KeyPress } from "@input/main";
import { Vector, Block } from "@physics/main";
import { Entity } from "../entity/Entity";

export class Camera extends Entity {
  /**
   * Get relative position of elements in camera
   *
   * @param entity
   * @returns
   */
  getRelativePosition(entity: Entity): Vector {
    return new Vector(
      new Block(entity.position.x.value - this.position.x.value),
      new Block(entity.position.y.value - this.position.y.value)
    );
  }

  /**
   * Change camera position according to the entity position
   *
   */
  anchorTo(entity: Entity) {
    this.position.x.value = entity.position.x.value - this.width.value / 2;
    this.position.y.value = entity.position.y.value - this.height.value / 2;
  }
}
