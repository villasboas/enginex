import { Game } from "../game/Game";

export class Engine {
  /**
   * Current frame rate value
   *
   */
  static frameRateValue: number = 60;

  static frameUpdateCallbacks: Array<() => void> = [];

  /**
   * Set global frame rate
   *
   * @param frameRate
   */
  static frameRate(frameRate: number): void {
    Engine.frameRateValue = frameRate;
  }

  /**
   * Get current frame rate in miliseconds.
   *
   * @returns
   */
  static getFrameRateAsMiliseconds(): number {
    return 1000 / Engine.frameRateValue;
  }

  static onFrameUpdate(callback: () => void): void {
    Engine.frameUpdateCallbacks.push(callback);
  }

  /**
   * Start the engine.
   *
   * @param game
   */
  static start(game: Game): void {
    console.log("Carregando...");
    setTimeout(() => {
      game.setup();
      console.log("Iniciando...");

      setInterval(() => {
        game.loop();

        Engine.frameUpdateCallbacks.forEach((callback) => callback());
      }, Engine.getFrameRateAsMiliseconds());
    }, 1000);
  }
}
