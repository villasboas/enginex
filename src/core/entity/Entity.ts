import { Block, Vector } from "@physics/main";

export type EntityProps = {
  position?: Vector;
  velocity?: Vector;
  acceleration?: Vector;
  width?: Block;
  height?: Block;
};

export abstract class Entity {
  /**
   * Entity position in map
   *
   */
  public position: Vector = new Vector(new Block(0), new Block(0));

  /**
   * Indicates if entity is collideable
   *
   */
  public collideable: boolean = false;

  /**
   * Entity velocity
   *
   */
  public velocity: Vector = new Vector(new Block(0), new Block(0));

  /**
   * Entity acceleration
   *
   */
  public acceleration: Vector = new Vector(new Block(0), new Block(0));

  /**
   * Entity width
   *
   */
  public width: Block = new Block(1);

  /**
   * Entity height
   *
   */
  public height: Block = new Block(1);

  /**
   * Indicates if entity is colliding with something
   *
   */
  public isColliding: boolean = false;

  /**
   * Constructor method
   *
   * @param props
   */
  constructor(props: EntityProps) {
    Object.assign(this, props);
  }

  /**
   * Set velocity vector
   *
   * @param velocity
   * @returns
   */
  setVelocity(velocity: Vector) {
    this.velocity = velocity;

    return this;
  }

  /**
   * Check if two entities colides
   *
   * @param entity
   */
  collides(entity: Entity): boolean {
    if (
      this.position.x.value < entity.position.x.value + entity.width.value &&
      this.position.x.value + this.width.value > entity.position.x.value &&
      this.position.y.value < entity.position.y.value + entity.height.value &&
      this.position.y.value + this.height.value > entity.position.y.value
    ) {
      return true;
    }

    return false;
  }

  /**
   * Move the entity in the world
   *
   */
  move() {
    this.position.x.value += this.velocity.x.value;
    this.position.y.value += this.velocity.y.value;
  }
}
