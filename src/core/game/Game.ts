import { Entity } from "@core/main";

export abstract class Game {
  private entities: Array<Entity> = [];

  pushEntity(entity: Entity): void {
    this.entities.push(entity);
  }

  abstract setup(): void;
  abstract loop(): void;
}
