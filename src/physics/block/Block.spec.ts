import { Block } from "./Block";

const makeSut = (value: number): Block => {
  return new Block(value);
};

describe("Block", () => {
  it("should add correct value", () => {
    const sut = makeSut(5);

    expect(sut.value).toBe(5);
  });

  it("should return correct value in pixels", () => {
    const sut = makeSut(5);

    expect(sut.toPixels()).toBe(5 * Block.RATIO);
  });

  it("should return a block from a pixels value", () => {
    const result = Block.fromPixels(100);

    expect(result.value).toBe(100 / Block.RATIO);
  });
});
