export class Block {
  /**
   * Global ratio transfer
   */
  static RATIO = 124;

  /**
   * Get current block value
   *
   * @returns
   */
  get value(): number {
    return this.blockValue;
  }

  /**
   * Set current block value
   *
   */
  set value(value: number) {
    this.blockValue = value;
  }

  /**
   * Constructor method
   *
   * @param value
   */
  constructor(public blockValue: number) {}

  /**
   * Get a block from pixels value
   *
   * @param value
   * @returns
   */
  static fromPixels(value: number): Block {
    return new Block(value / Block.RATIO);
  }

  /**
   * Check if current value is zero
   *
   * @returns
   */
  isZero() {
    return this.value === 0;
  }

  /**
   * Conver a block to pixels
   *
   * @returns
   */
  toPixels(): number {
    return this.value * Block.RATIO;
  }

  /**
   * Add a block value
   *
   * @param value
   * @returns
   */
  add(value: Block): Block {
    this.value += value.value;
    return this;
  }

  /**
   * Subtract a block value
   *
   * @param value
   * @returns
   */
  subtract(value: Block): Block {
    this.value -= value.value;
    return this;
  }

  /**
   * Multiply a block value
   *
   * @param value
   * @returns
   */
  multiply(value: Block): Block {
    this.value *= value.value;
    return this;
  }

  /**
   * Divide a block value
   *
   * @param value
   * @returns
   */
  divide(value: Block): Block {
    this.value /= value.value;
    return this;
  }
}
