import { Block } from "@physics/main";
import { Vector } from "./Vector";

const makeSut = (x, y) => {
  return new Vector(x, y);
};

describe("Vector", () => {
  it("should set correct x and y position", () => {
    const sut = makeSut(new Block(1), new Block(1));

    expect(sut.x.value).toBe(1);
    expect(sut.y.value).toBe(1);
  });
});
