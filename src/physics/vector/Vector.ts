import { Block } from "../block/Block";

export class Vector {
  /**
   * Constructor method
   *
   * @param x
   * @param y
   */
  constructor(public x: Block, public y: Block) {}

  /**
   * Set vector values
   *
   * @param vector
   * @returns
   */
  set(vector: Vector): Vector {
    this.x = vector.x;
    this.y = vector.y;
    return this;
  }

  /**
   * Check if vector is zero
   *
   * @returns
   */
  isZero(): boolean {
    return this.x.isZero() && this.y.isZero();
  }

  /**
   * Add a vector to another
   *
   * @param vector
   * @returns
   */
  add(vector: Vector): Vector {
    this.x.add(vector.x);
    this.y.add(vector.y);
    return this;
  }

  /**
   * Subtract a vector to another
   *
   * @param vector
   * @returns
   */
  subtract(vector: Vector): Vector {
    this.x.subtract(vector.x);
    this.y.subtract(vector.y);
    return this;
  }

  /**
   * Multiply a vector to another
   *
   * @param vector
   * @returns
   */
  multiply(value: Block): Vector {
    this.x.multiply(value);
    this.y.multiply(value);
    return this;
  }

  /**
   * Divide a vector to another
   *
   * @param vector
   * @returns
   */
  divide(value: Block): Vector {
    this.x.divide(value);
    this.y.divide(value);
    return this;
  }
}
