export enum DIRECTION {
  NORTH = "north",
  SOUTH = "south",
  EAST = "east",
  WEST = "west",
}

export * from "./vector/Vector";
export * from "./block/Block";
