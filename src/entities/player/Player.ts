import { Block, DIRECTION, Vector } from "@physics/main";
import { KeyPress } from "@input/main";
import { Entity } from "@core/main";
import {
  AssetsManager,
  Animation,
  Renderable,
  Sprite,
  Texture,
} from "@render/main";

export class Player extends Entity implements Renderable {
  public collideable: boolean = true;

  public facing: DIRECTION = DIRECTION.WEST;

  public get isWaking(): boolean {
    return !this.velocity.isZero();
  }

  public animations: any = {
    east: null,
    west: null,
    north: null,
    south: null,
  };

  constructor(options) {
    super(options);

    const sprites = new Sprite(AssetsManager.getTexture("player"), 4, 4);
    this.animations.south = new Animation(sprites, 0, 3);
    this.animations.west = new Animation(sprites, 4, 7);
    this.animations.east = new Animation(sprites, 8, 11);
    this.animations.north = new Animation(sprites, 12, 15);

    KeyPress.onKeyPress(KeyPress.ARROW_DOWN, () => {
      this.facing = DIRECTION.SOUTH;
      this.velocity.set(new Vector(new Block(0), new Block(0.1)));

      return () => this.stop();
    });

    KeyPress.onKeyPress(KeyPress.ARROW_UP, () => {
      this.facing = DIRECTION.NORTH;
      this.velocity.set(new Vector(new Block(0), new Block(-0.1)));

      return () => this.stop();
    });

    KeyPress.onKeyPress(KeyPress.ARROW_LEFT, () => {
      this.facing = DIRECTION.WEST;
      this.velocity.set(new Vector(new Block(-0.1), new Block(0)));

      return () => this.stop();
    });

    KeyPress.onKeyPress(KeyPress.ARROW_RIGHT, () => {
      this.facing = DIRECTION.EAST;
      this.velocity.set(new Vector(new Block(0.1), new Block(0)));

      return () => this.stop();
    });
  }

  stop() {
    this.velocity.set(new Vector(new Block(0), new Block(0)));
  }

  render(): Texture {
    const animation = this.animations[this.facing];
    this.position.add(this.velocity);

    let texture;
    if (!this.isWaking) {
      texture = animation.startFrame();
    } else {
      texture = animation.frame();
    }

    return {
      ...texture,
      strokeColor: this.isColliding ? "red" : "black",
    };
  }
}
