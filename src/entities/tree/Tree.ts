import { AssetsManager, Renderable, Texture } from "@render/main";
import { Entity } from "@core/main";

export class Tree extends Entity implements Renderable {
  public collideable: boolean = true;

  render(): Texture {
    const texture = AssetsManager.getTexture("tree");
    return {
      source: texture,
      sourceX: 0,
      sourceY: 0,
      sourceWidth: texture.width,
      sourceHeight: texture.height,
      strokeColor: this.isColliding ? "red" : "black",
    };
  }
}
