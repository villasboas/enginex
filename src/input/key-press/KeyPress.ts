export class KeyPress {
  static ARROW_LEFT: string = "ArrowLeft";
  static ARROW_RIGHT: string = "ArrowRight";
  static ARROW_UP: string = "ArrowUp";
  static ARROW_DOWN: string = "ArrowDown";

  static RELEASE: any = [];

  static PRESSED: any = [];

  static loop() {
    for (const key in KeyPress.PRESSED) {
      if (KeyPress.PRESSED[key]) {
        KeyPress.PRESSED[key].forEach((callback: any, index: number) => {
          const result = callback(key);
          if (result) {
            if (!KeyPress.RELEASE[key]) {
              KeyPress.RELEASE[key] = [];
            }
            KeyPress.RELEASE[key][index] = result;
          }
        });
      }
    }
  }

  static onKeyPress(key: string, callback: (key: string) => void): void {
    let fired = false;
    document.addEventListener("keydown", (event: KeyboardEvent) => {
      if (event.key !== key) {
        return;
      }

      if (!fired) {
        fired = true;
      } else return;

      if (KeyPress.PRESSED[event.key]) {
        KeyPress.PRESSED[event.key].push(callback);
      } else {
        KeyPress.PRESSED[event.key] = [callback];
      }
    });
    document.addEventListener("keyup", (event: KeyboardEvent) => {
      if (event.key !== key) {
        return;
      }

      fired = false;

      const releases = KeyPress.RELEASE[event.key];
      if (releases) {
        releases.forEach((callback: any) => callback());
      }

      delete KeyPress.RELEASE[event.key];
      delete KeyPress.PRESSED[event.key];
    });
  }
}
