import { Vector, Block } from "@physics/main";
import { Ground } from "../blocks/Ground";
import { Entity } from "@core/main";
import { Tree } from "@entities/tree/Tree";

const enemy = new Tree({
  position: new Vector(new Block(2), new Block(2)),
  height: new Block(2),
  width: new Block(2),
});

const enemy2 = new Tree({
  position: new Vector(new Block(-2), new Block(-2)),
  height: new Block(2),
  width: new Block(2),
});

export class World {
  public elements: Entity[] = [];

  static startX: Block = new Block(-256);
  static endX: Block = new Block(256);

  static startY: Block = new Block(-256);
  static endY: Block = new Block(256);

  constructor() {
    for (let x = World.startX.value; x <= World.endX.value; x++) {
      for (let y = World.startY.value; y <= World.endY.value; y++) {
        this.elements.push(
          new Ground({
            position: new Vector(new Block(x), new Block(y)),
          })
        );
      }
    }
    this.elements.push(enemy);
    this.elements.push(enemy2);
  }

  getElementsInRange(range: Entity): Entity[] {
    const elements: Entity[] = this.elements.filter((element) =>
      element.collides(range)
    );

    return elements;
  }
}
