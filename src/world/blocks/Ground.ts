import { AssetsManager, Renderable, Texture } from "@render/main";
import { Entity } from "@core/main";

export class Ground extends Entity implements Renderable {
  render(): Texture {
    const texture = AssetsManager.getTexture("ground");

    return {
      source: texture,
      sourceX: 0,
      sourceY: 0,
      sourceWidth: texture.width,
      sourceHeight: texture.height,
    };
  }
}
