import { Renderable } from "../contracts/Renderable";
import { Block, Vector } from "@physics/main";

export class Canvas {
  static DEBUG = true;

  private context: CanvasRenderingContext2D;

  public get width() {
    return this.canvas.width;
  }

  public get height() {
    return this.canvas.height;
  }

  constructor(public canvas: HTMLCanvasElement) {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    window.onresize = () => {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    };

    this.context = canvas.getContext("2d")!;
  }

  render(element: Renderable, dest: Vector, size: Vector) {
    const texture = element.render();
    this.context.drawImage(
      texture.source,
      texture.sourceX,
      texture.sourceY,
      texture.sourceWidth,
      texture.sourceHeight,
      dest.x.toPixels(),
      dest.y.toPixels(),
      size.x.toPixels(),
      size.y.toPixels()
    );

    if (Canvas.DEBUG) {
      const color = texture.strokeColor ?? "#000";
      this.context.beginPath();
      this.context.strokeStyle = color;
      this.context.rect(
        dest.x.toPixels(),
        dest.y.toPixels(),
        size.x.toPixels(),
        size.y.toPixels()
      );
      this.context.stroke();
    }
  }
}
