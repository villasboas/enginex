import { Sprite } from "./Sprite";

export class Animation {
  /**
   * Times animation was rendered
   *
   */
  public frames = 0;

  /**
   * Current displayed frame
   */
  public currentFrame;

  /**
   * Constructor method
   *
   * @param sprites
   * @param start
   * @param end
   * @param frameRate
   */
  constructor(
    public sprites: Sprite,
    public start: number,
    public end: number,
    public frameRate: number = 3
  ) {
    this.currentFrame = this.start;
  }

  /**
   * Get first animation frame
   *
   * @returns
   */
  startFrame() {
    return this.sprites.frame(this.start);
  }

  /**
   * Get next animation frame
   *
   * @returns
   */
  frame() {
    this.frames++;

    if (this.frames > this.frameRate) {
      this.currentFrame++;
      this.frames = 0;
    }

    if (this.currentFrame > this.end) {
      this.currentFrame = this.start;
    }

    return this.sprites.frame(this.currentFrame);
  }
}
