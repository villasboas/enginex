export type Texture = {
  source: any;
  sourceX: number;
  sourceY: number;
  sourceWidth: number;
  sourceHeight: number;
  strokeColor?: string;
};
