import { Texture } from "./Texture";

export class Sprite {
  /**
   * Sprite parsed frames
   *
   */
  public frames: Texture[] = [];

  /**
   * Constructor method
   *
   * @param source
   * @param cols
   * @param rows
   */
  constructor(source: HTMLImageElement, cols: number, rows: number) {
    const frameWidth = Math.floor(source.width / cols);
    const frameHeight = Math.floor(source.height / rows);

    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        const frame = {
          source: source,
          sourceX: col * frameWidth,
          sourceY: row * frameHeight,
          sourceWidth: frameHeight,
          sourceHeight: frameHeight,
        };

        this.frames.push(frame);
      }
    }
  }

  /**
   * Get a frame
   *
   * @param index
   * @returns
   */
  frame(index: number): Texture {
    return this.frames[index];
  }
}
