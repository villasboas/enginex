import { Texture } from "../texture/Texture";

export interface Renderable {
  render(): Texture;
}
