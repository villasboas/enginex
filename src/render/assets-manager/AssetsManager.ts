export class AssetsManager {
  /**
   * AssetsManager private instance
   */
  private static _instance: AssetsManager;

  /**
   * Available textures
   */
  public textures: Map<string, HTMLImageElement> = new Map<
    string,
    HTMLImageElement
  >();

  /**
   * Constructor method
   *
   */
  private constructor() {}

  /**
   * Get the instance of the AssetsManager
   *
   */
  public static get instance(): AssetsManager {
    if (!AssetsManager._instance) {
      AssetsManager._instance = new AssetsManager();
    }

    return AssetsManager._instance;
  }

  /**
   * Get a specific texture
   *
   * @param key
   * @returns
   */
  public static getTexture(key: string): HTMLImageElement {
    return AssetsManager.instance.textures.get(key)!;
  }

  /**
   * Add texture to manager
   *
   * @param key
   * @param source
   */
  public static addTexture(key: string, source: string): AssetsManager {
    const img = new Image();
    img.src = source;
    AssetsManager.instance.textures.set(key, img);

    return AssetsManager.instance;
  }
}
