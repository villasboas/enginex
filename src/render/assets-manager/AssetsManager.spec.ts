import { AssetsManager } from "./AssetsManager";

global.Image = jest.fn().mockImplementation(() => ({
  src: "",
}));

const makeSut = () => AssetsManager;

describe("AssetsManager", () => {
  it("should add a texture", () => {
    const sut = makeSut();

    sut.addTexture("test", "http://valid_test");

    expect(sut.instance.textures.get("test")).toBeDefined();
    expect(sut.instance.textures.get("test")?.src).toBe("http://valid_test");

    expect(sut.getTexture("test")).toBeInstanceOf(Object);
    expect(sut.getTexture("test")?.src).toBe("http://valid_test");
  });
});
