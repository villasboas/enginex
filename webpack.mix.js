const mix = require("laravel-mix");
const path = require("path");

mix
  .ts("src/main.ts", "dist")
  .ts("src/example/example.ts", "dist/example")
  .alias({
    "@core": path.join(__dirname, "src/core"),
    "@render": path.join(__dirname, "src/render"),
    "@world": path.join(__dirname, "src/world"),
    "@entities": path.join(__dirname, "src/entities"),
    "@physics": path.join(__dirname, "src/physics"),
    "@input": path.join(__dirname, "src/input"),
  })
  .copy("src/assets", "dist/assets")
  .setPublicPath("dist");
